function Parse_m2scordata

[FileName,PathName] = uigetfile('*.m2-scor-data','Select the m2-scor-data file');

pixelpitch_x = 4.4e-6;
pixelpitch_y = 4.4e-6;

z_positions = [];
x_widths = [];
y_widths = [];

frameidx = 0;
lambdafound = false;
fprintf('\nParsing %s...\n',[PathName FileName]);
fileID = fopen([PathName FileName],'r','ieee-le','windows-1252');
headerFinished = true;
tline = fgetl(fileID);
while ischar(tline)
  if(~lambdafound && startsWith(tline,'WaveLength='))
    lambda = str2double(tline(12:end))*1e-9;
    lambdafound = true;
    fprintf('Wavelength found to be %.0f nm\n',1e9*lambda);
  elseif startsWith(tline,'ZLocation=')
    new_z = str2double(tline(11:end))*20e-6;
    fprintf('Found z location at %.2f mm\n',1e3*new_z);
    z_positions = [z_positions , new_z];
  elseif startsWith(tline,'BeamWidth=')
    widths = sscanf(tline,'BeamWidth=%f,%f');
    x_widths = [x_widths widths(1)*pixelpitch_x];
    y_widths = [y_widths widths(2)*pixelpitch_y];
  elseif startsWith(tline,'                                                                                                   ')
    headerFinished = true;
  elseif headerFinished && (endsWith(tline,'[Header]') || endsWith(tline,'[FirewireCamera]') || endsWith(tline,'[Capture]') || endsWith(tline,'[FirewireCamera]') || endsWith(tline,'[M2]') || endsWith(tline,'[ResultsQuant]') || endsWith(tline,'[Aperture]'))
    headerFinished = false;
    if frameidx > 0
      fseek(fileID,-10-2*1600*1200,'cof');
      I(:,:,frameidx) = fliplr(fread(fileID,[1600 1200],'int16=>int16'));
      fseek(fileID,11,'cof');
    end
    frameidx = frameidx + 1;
  end
  
  tline = fgetl(fileID);
end

fseek(fileID,-2*1600*1200,'cof');
I(:,:,frameidx) = fliplr(fread(fileID,[1600 1200],'int16=>int16'));

fclose(fileID);

fprintf('Saving data to %s...\n',[PathName FileName(1:end-12) 'mat']);
save([PathName FileName(1:end-12) 'mat'], 'I', 'z_positions', 'lambda', 'x_widths', 'y_widths','pixelpitch_x','pixelpitch_y');
fprintf('Done\n\n');

end