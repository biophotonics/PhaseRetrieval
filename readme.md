How to use:

The Phaseretrieval.m function can read a .mat file of input intensities and
perform phase retrieval on them, and save the output. See the comments in the
Phaseretrieval.m file for how the input .mat file should look.

The FitCaustics_SaveImages.m function can read a .mat file, either raw 
input data or phase retrieved output data, plot and save the image frames 
and fit caustics to the widths. Images are saved in a multi-page .tiff file
which can be viewed, e.g., with Irfanview.

The Parse_m2scordata.m function can read a Spiricon M2-200s profiler 
proprietary data file, extract the intensity profile data and save it in a 
.mat file suitable for input to Phaseretrieval.m.

The ImageImport.m function can be used to read intensity information from
image files and likewise save it in a .mat file suitable for input to
PhaseRetrieval.m