/********************************************
 * PRiterator.c, in the C programming language, written for MATLAB MEX function generation
 * Can be compiled with GCC using
 * "mex COPTIMFLAGS='$COPTIMFLAGS -Ofast -fopenmp -std=c11 -Wall' LDOPTIMFLAGS='$LDOPTIMFLAGS -Ofast -fopenmp -std=c11 -Wall' -outdir private .\src\PRiterator.c ".\src\libfftw3f-3.lib" -R2018a"
 * ... or the Microsoft Visual C++ compiler (MSVC) with
 * "copyfile ./src/PRiterator.c ./src/PRiterator.cpp; mex COMPFLAGS='/Zp8 /GR /EHs /nologo /MD /openmp /W4 /WX /wd4204 /wd4100' -outdir private .\src\PRiterator.cpp ".\src\libfftw3f-3.lib" -R2018a"
 * 
 * The source code in this file is written is such a way that it is
 * compilable by either C or C++ compilers, either with GCC, MSVC or
 * the Nvidia CUDA compiler called NVCC, which is based on MSVC. To
 * compile with CUDA GPU acceleration support, you must have MSVC
 * installed. As of January 2020, mexcuda does not work with MSVC 2019,
 * so I'd recommend MSVC 2017. You also need the Parallel Computing
 * Toolbox, which you will find in the MATLAB addon manager. To compile, run:
 * "copyfile ./src/PRiterator.c ./src/PRiterator_CUDA.cu; mexcuda -lcufft COMPFLAGS='-use_fast_math -res-usage $COMPFLAGS' -outdir private .\src\PRiterator_CUDA.cu -R2018a"
 ********************************************/
// printf("Reached line %d...\n",__LINE__);mexEvalString("drawnow;");mexEvalString("drawnow;");mexEvalString("drawnow;"); // For inserting into code for debugging purposes
// long long t_old = getMicroSeconds(), t_new;
// t_new = getMicroSeconds(); printf("Reaching line %d takes %d ms\n",__LINE__,(t_new-t_old)/1000); t_old = t_new;

#include "mex.h"
#include <math.h>

#ifdef _OPENMP
  #include "omp.h"
#endif
#ifdef __NVCC__
  #include <windows.h>
  #include <cufft.h>
  #include <thrust/complex.h>
  typedef thrust::complex<float> floatcomplex;
  #define CEXPF(x)  (thrust::exp(x))
  #define MIN(x,y)  (min(x,y))
  #define MAX(x,y)  (max(x,y))
  #define CREALF(x) (x.real())
  #define CIMAGF(x) (x.imag())
  #define CABSF(x)  (thrust::abs(x))
  #define CARGF(x)  (thrust::arg(x))
#else
  #include "fftw-3.3.5-dll64/fftw3.h"
  #ifdef __GNUC__ // This is defined for GCC and CLANG but not for Microsoft Visual C++ compiler
    #include <time.h>
    #include <complex.h>
    typedef float complex floatcomplex;
    #define MIN(a,b) ({__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a <  _b? _a: _b;})
    #define MAX(a,b) ({__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a >= _b? _a: _b;})
    #define CREALF(x) (crealf(x))
    #define CIMAGF(x) (cimagf(x))
    #define CABSF(x)  (cabsf(x))
    #define CARGF(x)  (cargf(x))
  #else
    #include <windows.h>
    #include <algorithm>
    #include <complex>
    typedef std::complex<float> floatcomplex;
    #define MIN(x,y) (min(x,y))
    #define MAX(x,y) (max(x,y))
    #define CREALF(x) (std::real(x))
    #define CIMAGF(x) (std::imag(x))
    #define CABSF(x)  (std::abs(x))
    #define CARGF(x)  (std::arg(x))
  #endif
#endif

struct debug {
  double             dbls[3];
  unsigned long long ulls[3];
};

struct parameters {
  floatcomplex *E_i;
  floatcomplex *A_i;
  floatcomplex *E_t;
  floatcomplex *A_t;
  floatcomplex *E_o;
  floatcomplex *A_o;
  floatcomplex *fft_I_conj;
  floatcomplex *I_t; // Will actually only contain real data but the FFTs become easier if we represent it as complex
  float *E_mag_meas;
  long *iz_PR_array;
  long Nx;
  long Ny;
  long Nz;
  long Nz_PR;
  long *frameshifts;
  long *frameshifts_out;
  double *fidelities;
  float gain;
  floatcomplex *prop_kernel;
  double *attenuations;
  double P_i;
};

#define printmat_floatcomplex(a,b,c) {_printmat_floatcomplex(a,b,c,__LINE__);}
void _printmat_floatcomplex(floatcomplex *A, long Nx, long Ny, int line) {
  printf("Printing matrix at line %d, memory address %llu:\n",line,(unsigned long long)A);
  for(long j=0;j<Nx;j++) {
    for(long i=0;i<Ny;i++) printf("%f + %fi  ",CREALF(A[j + i*Nx]),CIMAGF(A[j + i*Nx]));
    printf("\n");
  }
  printf("\n");
}

#if defined(__CUDA_ARCH__) && __CUDA_ARCH__ < 600
__device__ double atomicAdd(double* address, double val) {
  unsigned long long int* address_as_ull = (unsigned long long int*)address;
  unsigned long long int old = *address_as_ull, assumed;

  do {
    assumed = old;
    old = atomicCAS(address_as_ull, assumed,__double_as_longlong(val + __longlong_as_double(assumed)));
  } while (assumed != old);
  return __longlong_as_double(old);
}
#endif

#ifdef __NVCC__ // If compiling for CUDA
__device__ __forceinline__ float atomicMaxFloat(float *addr, float value) {
    float old;
    old = (value >= 0) ? __int_as_float(atomicMax((int *)addr, __float_as_int(value))) :
         __uint_as_float(atomicMin((unsigned int *)addr, __float_as_uint(value)));
    return old;
}

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line) {
   if (code != cudaSuccess) {
      printf("GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      mexEvalString("drawnow;");
      while(true) {;}
   }
}

void createDeviceStructs(struct parameters *P, struct parameters **P_devptr, struct parameters *P_dev_localcopy,
                         struct debug *D, struct debug **D_devptr) {
  *P_dev_localcopy = *P;
  gpuErrchk(cudaMalloc(&P_dev_localcopy->frameshifts,2*P->Nz_PR*sizeof(long)));
  gpuErrchk(cudaMemcpy(P_dev_localcopy->frameshifts,P->frameshifts,2*P->Nz_PR*sizeof(long),cudaMemcpyHostToDevice));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->frameshifts_out,2*P->Nz_PR*sizeof(long)));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->iz_PR_array,P->Nz_PR*sizeof(long)));
  gpuErrchk(cudaMemcpy(P_dev_localcopy->iz_PR_array,P->iz_PR_array,P->Nz_PR*sizeof(long),cudaMemcpyHostToDevice));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->fidelities,P->Nz*sizeof(double)));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->attenuations,P->Nz*sizeof(double)));
  gpuErrchk(cudaMemset(P_dev_localcopy->attenuations,0,P->Nz*sizeof(double)));

  gpuErrchk(cudaMalloc(&P_dev_localcopy->E_i,P->Nx*P->Ny*sizeof(floatcomplex)));
  gpuErrchk(cudaMemcpy(P_dev_localcopy->E_i,P->E_i,P->Nx*P->Ny*sizeof(floatcomplex),cudaMemcpyHostToDevice));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->E_o,P->Nx*P->Ny*sizeof(floatcomplex)));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->A_o,P->Nx*P->Ny*sizeof(floatcomplex)));
  gpuErrchk(cudaMemset(P_dev_localcopy->A_o,0,P->Nx*P->Ny*sizeof(floatcomplex)));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->E_t,P->Nx*P->Ny*P->Nz*sizeof(floatcomplex)));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->A_t,P->Nx*P->Ny*P->Nz*sizeof(floatcomplex)));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->I_t,P->Nx*P->Ny*P->Nz*sizeof(floatcomplex)));
  P_dev_localcopy->A_i = P_dev_localcopy->E_o;

  gpuErrchk(cudaMalloc(&P_dev_localcopy->E_mag_meas,P->Nx*P->Ny*P->Nz_PR*sizeof(float)));
  gpuErrchk(cudaMemcpy(P_dev_localcopy->E_mag_meas,P->E_mag_meas,P->Nx*P->Ny*P->Nz_PR*sizeof(float),cudaMemcpyHostToDevice));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->prop_kernel,P->Nx*P->Ny*P->Nz*sizeof(floatcomplex)));
  gpuErrchk(cudaMemcpy(P_dev_localcopy->prop_kernel,P->prop_kernel,P->Nx*P->Ny*P->Nz*sizeof(floatcomplex),cudaMemcpyHostToDevice));
  gpuErrchk(cudaMalloc(&P_dev_localcopy->fft_I_conj,P->Nx*P->Ny*P->Nz*sizeof(floatcomplex)));
  gpuErrchk(cudaMemcpy(P_dev_localcopy->fft_I_conj,P->fft_I_conj,P->Nx*P->Ny*P->Nz*sizeof(floatcomplex),cudaMemcpyHostToDevice));

  gpuErrchk(cudaMalloc(P_devptr, sizeof(struct parameters)));
  gpuErrchk(cudaMemcpy(*P_devptr,P_dev_localcopy,sizeof(struct parameters),cudaMemcpyHostToDevice));

  struct debug D_tempvar = *D;
  gpuErrchk(cudaMalloc(D_devptr, sizeof(struct debug)));
  gpuErrchk(cudaMemcpy(*D_devptr,&D_tempvar,sizeof(struct debug),cudaMemcpyHostToDevice));
}

void retrieveAndFreeDeviceStructs(struct parameters *P, struct parameters *P_dev,
                                  struct debug *D, struct debug *D_dev) {
  struct parameters P_temp; gpuErrchk(cudaMemcpy(&P_temp, P_dev, sizeof(struct parameters),cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy(P->E_o, P_temp.E_o, P->Nx*P->Ny*sizeof(floatcomplex),cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy(P->frameshifts_out, P_temp.frameshifts_out, 2*P->Nz_PR*sizeof(long),cudaMemcpyDeviceToHost));

  gpuErrchk(cudaMemcpy(P->attenuations, P_temp.attenuations, P->Nz*sizeof(double),cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy(P->fidelities, P_temp.fidelities, P->Nz*sizeof(double),cudaMemcpyDeviceToHost));

  gpuErrchk(cudaFree(P_temp.frameshifts));
  gpuErrchk(cudaFree(P_temp.frameshifts_out));
  gpuErrchk(cudaFree(P_temp.iz_PR_array));
  gpuErrchk(cudaFree(P_temp.E_i));
  gpuErrchk(cudaFree(P_temp.E_o));
  gpuErrchk(cudaFree(P_temp.A_o));
  gpuErrchk(cudaFree(P_temp.E_t));
  gpuErrchk(cudaFree(P_temp.A_t));
  gpuErrchk(cudaFree(P_temp.I_t));

  gpuErrchk(cudaFree(P_temp.E_mag_meas));
  gpuErrchk(cudaFree(P_temp.prop_kernel));
  gpuErrchk(cudaFree(P_temp.fft_I_conj));

  gpuErrchk(cudaFree(P_temp.attenuations));
  gpuErrchk(cudaFree(P_temp.fidelities));

  gpuErrchk(cudaMemcpy(D, D_dev, sizeof(struct debug),cudaMemcpyDeviceToHost));
  gpuErrchk(cudaFree(D_dev));
}
#endif

#ifdef __NVCC__ // If compiling for CUDA
__host__ __device__
#endif
float sqrf(float x) {return x*x;}

#ifdef __NVCC__ // If compiling for CUDA
__global__
#endif
void applyGainCalcP(struct parameters *P) {
  #ifdef __NVCC__
  long threadNum = threadIdx.x + blockIdx.x*blockDim.x;
  long nThreads = gridDim.x*blockDim.x;
  for(long i=threadNum;i<P->Nx*P->Ny;i+=nThreads) {
    P->A_i[i] *= P->gain;
    atomicAdd(&P->P_i,(double)(sqrf(CREALF(P->A_i[i])) + sqrf(CIMAGF(P->A_i[i])))); // Input power in frequency space (Nx*Ny times larger than in real space)
  }
  #else
  for(long i=0;i<P->Nx*P->Ny;i++) {
    P->A_i[i] *= P->gain;
    P->P_i += (double)(sqrf(CREALF(P->A_i[i])) + sqrf(CIMAGF(P->A_i[i]))); // Input power in frequency space (Nx*Ny times larger than in real space)
  }
  #endif
}

void propagateRefToFrame(struct parameters *P, long iz) {
  for(long i=0;i<P->Nx*P->Ny;i++) {
    P->A_t[i] = P->A_i[i]*P->prop_kernel[i + iz*P->Nx*P->Ny]/(float)(P->Nx*P->Ny); // Divide by Nx*Ny is because FFTW yields the non-normalized FTs
  }
}

#ifdef __NVCC__ // If compiling for CUDA
__global__
void propagateRefToFrames(struct parameters *P) {
  long threadNum = threadIdx.x + blockIdx.x*blockDim.x;
  long nThreads = gridDim.x*blockDim.x;
  for(long i=threadNum;i<P->Nx*P->Ny*P->Nz;i+=nThreads) {
    P->A_t[i] = P->A_i[i%(P->Nx*P->Ny)]*P->prop_kernel[i]/(float)(P->Nx*P->Ny); // Divide by Nx*Ny is because cuFFT yields the non-normalized FTs
  }
}
#endif

void calcI(struct parameters *P) {
  double I_sumsquares = 0;
  for(long i=0;i<P->Nx*P->Ny;i++) {
    P->I_t[i] = sqrf(CREALF(P->E_t[i])) + sqrf(CIMAGF(P->E_t[i]));
    I_sumsquares += (double)sqrf(CREALF(P->I_t[i]));
  }
  for(long i=0;i<P->Nx*P->Ny;i++) P->I_t[i] /= (float)sqrt(I_sumsquares);
}

#ifdef __NVCC__ // If compiling for CUDA
__global__
void calcIs(struct parameters *P) {
  for(long iz=blockIdx.x;iz<P->Nz;iz+=gridDim.x) {
    __shared__ double I_sumsquares;
    if(!threadIdx.x) I_sumsquares = 0;
    __syncthreads();
    for(long ixy=threadIdx.x;ixy<P->Nx*P->Ny;ixy+=blockDim.x) {
      long i = ixy + iz*P->Nx*P->Ny;
      P->I_t[i] = sqrf(CREALF(P->E_t[i])) + sqrf(CIMAGF(P->E_t[i]));
      atomicAdd(&I_sumsquares,(double)sqrf(CREALF(P->I_t[i])));
    }
    __syncthreads();
    for(long ixy=threadIdx.x;ixy<P->Nx*P->Ny;ixy+=blockDim.x) {
      long i = ixy + iz*P->Nx*P->Ny;
      P->I_t[i] /= (float)sqrt(I_sumsquares);
    }
  }
}
#endif

void calcfftprod(struct parameters *P, long iz) {
  for(long i=0;i<P->Nx*P->Ny;i++) {
    P->A_t[i] *= P->fft_I_conj[i + iz*P->Nx*P->Ny];
  }
}

#ifdef __NVCC__ // If compiling for CUDA
__global__
void calcfftprods(struct parameters *P) {
  long threadNum = threadIdx.x + blockIdx.x*blockDim.x;
  long nThreads = gridDim.x*blockDim.x;
  for(long i=threadNum;i<P->Nx*P->Ny*P->Nz;i+=nThreads) {
    P->A_t[i] *= P->fft_I_conj[i];
  }
}
#endif

void findmaxxcorr(struct parameters *P, long iz) {
  float maxval = 0;
  for(long i=0;i<P->Nx*P->Ny;i++) {
    maxval = MAX(maxval,sqrf(CREALF(P->I_t[i])) + sqrf(CIMAGF(P->I_t[i]))); // Here, I_t is where the xcorrelation coefficients are stored
  }
  P->fidelities[iz] = (double)maxval/sqrf((float)(P->Nx*P->Ny));
}

#ifdef __NVCC__ // If compiling for CUDA
__global__
void findmaxxcorrs(struct parameters *P) {
  for(long iz=blockIdx.x;iz<P->Nz;iz+=gridDim.x) {
    __shared__ float maxval;
    if(!threadIdx.x) maxval = 0;
    __syncthreads();
    for(long i=threadIdx.x;i<P->Nx*P->Ny;i+=blockDim.x) {
      atomicMaxFloat(&maxval,sqrf(CREALF(P->I_t[i + iz*P->Nx*P->Ny])) + sqrf(CIMAGF(P->I_t[i + iz*P->Nx*P->Ny]))); // Here, I_t is where the xcorrelation coefficients are stored
    }
    __syncthreads();
    if(!threadIdx.x) P->fidelities[iz] = (double)maxval/sqrf(P->Nx*P->Ny);
  }
}
#endif

void findBestShift(struct parameters *P, long iz_PR, bool shiftframes) {
  if(shiftframes) {
    // Try different frameshifts to find the one that has highest power of the output (lowest attenuation)
    float maxGoodness = 0;
    for(long ixshift = P->frameshifts[2*iz_PR] - 2; ixshift<P->frameshifts[2*iz_PR] + 2; ++ixshift) {
      for(long iyshift = P->frameshifts[2*iz_PR+1] - 2; iyshift<P->frameshifts[2*iz_PR+1] + 2; ++iyshift) {
        float goodness = 0;
        for(long i=0;i<P->Nx*P->Ny;i++) {
          // i is the index in the PR frame, not the measured frame
          long ix_meas = i%P->Nx - ixshift;
          long iy_meas = i/P->Nx - iyshift;
          if(ix_meas >= 0 && ix_meas < P->Nx && iy_meas >= 0 && iy_meas < P->Ny) {
            long i_meas = ix_meas + iy_meas*P->Nx;
            goodness += MIN(P->E_mag_meas[i_meas + iz_PR*P->Nx*P->Ny]/sqrtf(sqrf(CREALF(P->E_t[i])) + sqrf(CIMAGF(P->E_t[i]))) , 1.0f);
          }
        }
        if(goodness > maxGoodness) {
          maxGoodness = goodness;
          P->frameshifts_out[2*iz_PR  ] = ixshift;
          P->frameshifts_out[2*iz_PR+1] = iyshift;
        }
      }
    }
  } else {
    P->frameshifts_out[2*iz_PR  ] = P->frameshifts[2*iz_PR  ];
    P->frameshifts_out[2*iz_PR+1] = P->frameshifts[2*iz_PR+1];
  }
}

#ifdef __NVCC__ // If compiling for CUDA
__global__
void findBestShifts(struct parameters *P, bool shiftframes) {
  // Try different frameshifts to find the one that has highest power of the output (lowest attenuation)
  for(long iz_PR=blockIdx.x;iz_PR<P->Nz_PR;iz_PR+=gridDim.x) {
    long iz = P->iz_PR_array[iz_PR];
    if(shiftframes) {
      __shared__ float maxGoodness;
      if(!threadIdx.x) maxGoodness = 0;
      for(long ixshift = P->frameshifts[2*iz_PR] - 2; ixshift<P->frameshifts[2*iz_PR] + 2; ++ixshift) {
        for(long iyshift = P->frameshifts[2*iz_PR+1] - 2; iyshift<P->frameshifts[2*iz_PR+1] + 2; ++iyshift) {
          __shared__ float goodness;
          if(!threadIdx.x) goodness = 0;
          __syncthreads();
          for(long i=threadIdx.x;i<P->Nx*P->Ny;i+=blockDim.x) {
            // i is the index in the PR frame, not the measured frame
            long ixyz = i + iz*P->Nx*P->Ny;
            long ix_meas = i%P->Nx - ixshift;
            long iy_meas = i/P->Nx - iyshift;
            if(ix_meas >= 0 && ix_meas < P->Nx && iy_meas >= 0 && iy_meas < P->Ny) {
              long i_meas = ix_meas + iy_meas*P->Nx;
              atomicAdd(&goodness,MIN(P->E_mag_meas[i_meas + iz_PR*P->Nx*P->Ny]/sqrtf(sqrf(CREALF(P->E_t[ixyz])) + sqrf(CIMAGF(P->E_t[ixyz]))) , 1.0f));
            }
          }
          __syncthreads();
          if(!threadIdx.x && goodness > maxGoodness) {
            maxGoodness = goodness;
            P->frameshifts_out[2*iz_PR  ] = ixshift;
            P->frameshifts_out[2*iz_PR+1] = iyshift;
          }
        }
      }
    } else if(!threadIdx.x) {
      P->frameshifts_out[2*iz_PR  ] = P->frameshifts[2*iz_PR  ];
      P->frameshifts_out[2*iz_PR+1] = P->frameshifts[2*iz_PR+1];
    }
  }
}
#endif

void attenuateField(struct parameters *P, long iz_PR) {
  long ixshift = P->frameshifts_out[2*iz_PR    ];
  long iyshift = P->frameshifts_out[2*iz_PR + 1];
  for(long i=0;i<P->Nx*P->Ny;i++) {
    // i is the index in the PR frame, not the measured frame
    long ix_meas = i%P->Nx - ixshift;
    long iy_meas = i/P->Nx - iyshift;
    if(ix_meas >= 0 && ix_meas < P->Nx && iy_meas >= 0 && iy_meas < P->Ny) {
      long i_meas = ix_meas + iy_meas*P->Nx;
      P->E_t[i] *= MIN(P->E_mag_meas[i_meas + iz_PR*P->Nx*P->Ny]/sqrtf(sqrf(CREALF(P->E_t[i])) + sqrf(CIMAGF(P->E_t[i]))) , 1.0f);
    } else P->E_t[i] = 0;
  }
}

#ifdef __NVCC__ // If compiling for CUDA
__global__
void attenuateFields(struct parameters *P, struct debug *D) {
  long threadNum = threadIdx.x + blockIdx.x*blockDim.x;
  long nThreads = gridDim.x*blockDim.x;
  for(long ixyz_PR=threadNum;ixyz_PR<P->Nx*P->Ny*P->Nz_PR;ixyz_PR+=nThreads) {
    long i = ixyz_PR%(P->Nx*P->Ny);
    long iz_PR = ixyz_PR/(P->Nx*P->Ny);
    long iz = P->iz_PR_array[iz_PR];
    long ixyz = i + iz*P->Nx*P->Ny;
    long ixshift = P->frameshifts_out[2*iz_PR    ];
    long iyshift = P->frameshifts_out[2*iz_PR + 1];
    long ix_meas = i%P->Nx - ixshift;
    long iy_meas = i/P->Nx - iyshift;
    if(ix_meas >= 0 && ix_meas < P->Nx && iy_meas >= 0 && iy_meas < P->Ny) {
      long i_meas = ix_meas + iy_meas*P->Nx;
      P->E_t[ixyz] *= MIN(P->E_mag_meas[i_meas + iz_PR*P->Nx*P->Ny]/sqrtf(sqrf(CREALF(P->E_t[ixyz])) + sqrf(CIMAGF(P->E_t[ixyz]))) , 1.0f);
    } else P->E_t[ixyz] = 0;
  }
}
#endif

void propagateFrameToRef(struct parameters *P, long iz_PR) {
  long iz = P->iz_PR_array[iz_PR];
  for(long i=0;i<P->Nx*P->Ny;i++) {
    P->attenuations[iz] += (double)(sqrf(CREALF(P->A_t[i])) + sqrf(CIMAGF(P->A_t[i])));
    #ifdef _OPENMP
    float *Re = (float *)&P->A_o[i]; // Address of real part of A_o[i]
    float *Im = Re + 1; // +1 means go one float-width (4 bytes) up in address, which gives us the imaginary part of A_o[i]
    floatcomplex val = P->A_t[i]/P->prop_kernel[i + iz*P->Nx*P->Ny]/(float)(P->Nx*P->Ny*P->Nz_PR); // Divide by Nx*Ny is because FFTW yields the non-normalized FFTs. Dividing by Nz_PR is because we need to get the mean of all the contributions from all the frames involved in phase retrieval
    #pragma omp atomic
    *Re += CREALF(val);
    #pragma omp atomic
    *Im += CIMAGF(val);
    #else
    P->A_o[i] += P->A_t[i]/P->prop_kernel[i + iz*P->Nx*P->Ny]/(float)(P->Nx*P->Ny*P->Nz_PR);
    #endif
  }
  P->attenuations[iz] = 1 - P->attenuations[iz]/P->P_i;
}

#ifdef __NVCC__ // If compiling for CUDA
__global__
void propagateFramesToRef(struct parameters *P) {
  for(long iz_PR=blockIdx.x;iz_PR<P->Nz_PR;iz_PR+=gridDim.x) {
    long iz = P->iz_PR_array[iz_PR];
    for(long i=threadIdx.x;i<P->Nx*P->Ny;i+=blockDim.x) {
      long ixyz = i + iz*P->Nx*P->Ny;
      atomicAdd(&P->attenuations[iz],(double)(sqrf(CREALF(P->A_t[ixyz])) + sqrf(CIMAGF(P->A_t[ixyz]))));
      float *Re = (float *)&P->A_o[i]; // Address of real part of A_o[i]
      float *Im = Re + 1; // +1 means go one float-width (4 bytes) up in address, which gives us the imaginary part of A_o[i]
      floatcomplex val = P->A_t[ixyz]/P->prop_kernel[ixyz]/(float)(P->Nx*P->Ny*P->Nz_PR); // Divide by Nx*Ny is because FFTW yields the non-normalized FFTs. Dividing by Nz_PR is because we need to get the mean of all the contributions from all the frames involved in phase retrieval
      atomicAdd(Re,CREALF(val));
      atomicAdd(Im,CIMAGF(val));
    }
    __syncthreads();
    if(!threadIdx.x) P->attenuations[iz] = 1 - P->attenuations[iz]/P->P_i;
  }
}
#endif

long long getMicroSeconds() {
  #ifdef __GNUC__
  struct timespec time; clock_gettime(CLOCK_MONOTONIC, &time);
  return time.tv_sec*1000000 + time.tv_nsec/1000;
  #else
  static LARGE_INTEGER freq; QueryPerformanceFrequency(&freq);
  LARGE_INTEGER pfmcntr; QueryPerformanceCounter(&pfmcntr);
  return pfmcntr.QuadPart*1000000/freq.QuadPart;
  #endif
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, mxArray const *prhs[]) {
  struct parameters P_var;
  struct parameters *P = &P_var;
  mwSize const *dimPtr = mxGetDimensions(mxGetField(prhs[1],0,"prop_kernel"));
  P->Nx = (long)dimPtr[0];
  P->Ny = (long)dimPtr[1];
  P->Nz = (long)dimPtr[2];
  P->Nz_PR = (long)mxGetN(mxGetField(prhs[1],0,"iz_PR_array"));

  P->P_i = 0;
  P->frameshifts = (long *)mxGetData(prhs[2]);
  P->gain = *(float *)mxGetData(mxGetField(prhs[1],0,"gain"));
  P->prop_kernel = (floatcomplex *)mxGetData(mxGetField(prhs[1],0,"prop_kernel"));
  P->E_mag_meas = (float *)mxGetData(mxGetField(prhs[1],0,"E_mag_meas"));
  P->fft_I_conj = (floatcomplex *)mxGetData(mxGetField(prhs[1],0,"fft_I_conj"));
  P->iz_PR_array = (long *)mxGetData(mxGetField(prhs[1],0,"iz_PR_array"));
  bool shiftframes = mxIsLogicalScalarTrue(mxGetField(prhs[1],0,"shiftframes"));
  bool plotProgress = mxIsLogicalScalarTrue(mxGetField(prhs[1],0,"plotProgress"));
  P->E_i = (floatcomplex *)mxGetData(prhs[0]);
  size_t dimPtr2[2] = {dimPtr[0],dimPtr[1]};
  P->E_o = (floatcomplex *)mxGetData(plhs[0] = mxCreateNumericArray(2,dimPtr2,mxSINGLE_CLASS,mxCOMPLEX));
  P->A_i = P->E_o; // To conserve some memory, I can let A_i use the same memory space as E_o, since they are not needed at the same time
  double *phaseRMS = (double *)mxGetData(plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL));
  double *magnitudeRMS = (double *)mxGetData(plhs[2] = mxCreateDoubleMatrix(1,1,mxREAL));
  double *magEdiffRMS = (double *)mxGetData(plhs[3] = mxCreateDoubleMatrix(1,1,mxREAL));
  P->attenuations = (double *)mxGetData(plhs[4] = mxCreateDoubleMatrix(P->Nz,1,mxREAL));
  P->fidelities = (double *)mxGetData(plhs[6] = mxCreateDoubleMatrix(P->Nz,1,mxREAL));
  size_t dimPtr3[2] = {2,(size_t)P->Nz_PR};
  P->frameshifts_out = (long *)mxGetData(plhs[5] = mxCreateNumericArray(2,dimPtr3,mxINT32_CLASS,mxREAL));
  #ifdef __NVCC__
  int nBlocks, nThreadsPerBlock; gpuErrchk(cudaOccupancyMaxPotentialBlockSize(&nBlocks,&nThreadsPerBlock,&applyGainCalcP,0,0));
  struct parameters P_dev_localcopy_var;
  struct parameters *P_dev, *P_dev_localcopy = &P_dev_localcopy_var;
  struct debug D_var = {{0.0,0.0,0.0},{0,0,0}};
  struct debug *D = &D_var, *D_dev;
  createDeviceStructs(P, &P_dev, P_dev_localcopy, D, &D_dev);
  cufftHandle plan_nonbatch, plan_batch;
  
  int dimPtr4[2] = {P->Ny,P->Nx};
  cufftPlanMany(&plan_nonbatch, 2, dimPtr4, NULL, 1, P->Nx*P->Ny, NULL, 1, P->Nx*P->Ny, CUFFT_C2C,     1);
  cufftPlanMany(&plan_batch   , 2, dimPtr4, NULL, 1, P->Nx*P->Ny, NULL, 1, P->Nx*P->Ny, CUFFT_C2C, P->Nz);

  cufftExecC2C(plan_nonbatch, (cufftComplex *)P_dev_localcopy->E_i, (cufftComplex *)P_dev_localcopy->A_i, CUFFT_FORWARD);
  applyGainCalcP<<<nBlocks,nThreadsPerBlock>>>(P_dev);
  propagateRefToFrames<<<nBlocks,nThreadsPerBlock>>>(P_dev);
  cufftExecC2C(plan_batch, (cufftComplex *)P_dev_localcopy->A_t, (cufftComplex *)P_dev_localcopy->E_t, CUFFT_INVERSE);

  if(plotProgress) {
    // Cross correlation to find fidelities
    calcIs<<<nBlocks,nThreadsPerBlock>>>(P_dev);
    cufftExecC2C(plan_batch, (cufftComplex *)P_dev_localcopy->I_t, (cufftComplex *)P_dev_localcopy->A_t, CUFFT_FORWARD);
    calcfftprods<<<nBlocks,nThreadsPerBlock>>>(P_dev);
    cufftExecC2C(plan_batch, (cufftComplex *)P_dev_localcopy->A_t, (cufftComplex *)P_dev_localcopy->I_t, CUFFT_INVERSE);
    findmaxxcorrs<<<nBlocks,nThreadsPerBlock>>>(P_dev); // This calculates the fidelities
    // Cross correlation done
  }

  findBestShifts<<<nBlocks,nThreadsPerBlock>>>(P_dev,shiftframes);
  attenuateFields<<<nBlocks,nThreadsPerBlock>>>(P_dev,D_dev);
  cufftExecC2C(plan_batch, (cufftComplex *)P_dev_localcopy->E_t, (cufftComplex *)P_dev_localcopy->A_t, CUFFT_FORWARD);
  propagateFramesToRef<<<nBlocks,nThreadsPerBlock>>>(P_dev);
  cufftExecC2C(plan_nonbatch, (cufftComplex *)P_dev_localcopy->A_o, (cufftComplex *)P_dev_localcopy->E_o, CUFFT_INVERSE);

  cufftDestroy(plan_nonbatch);
  cufftDestroy(plan_batch);
  retrieveAndFreeDeviceStructs(P, P_dev, D, D_dev);
  #else
  P->A_o = (floatcomplex *)calloc(P->Nx*P->Ny,sizeof(floatcomplex));
  
  fftwf_plan p_BACKWARD = fftwf_plan_dft_2d(P->Ny, P->Nx, (fftwf_complex *)P->A_o, (fftwf_complex *)P->E_o, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftwf_plan p_FORWARD = fftwf_plan_dft_2d(P->Ny, P->Nx, (fftwf_complex *)P->E_i, (fftwf_complex *)P->A_i, FFTW_FORWARD, FFTW_ESTIMATE);

  fftwf_execute(p_FORWARD);
  applyGainCalcP(P);

  #ifdef _OPENMP
  long nThreads = omp_get_num_procs();
  #pragma omp parallel num_threads(nThreads)
  #endif
  {
    struct parameters Pt_val = *P; // Thread-specific copy
    struct parameters *Pt = &Pt_val;
    Pt->E_t = (floatcomplex *)malloc(Pt->Nx*Pt->Ny*sizeof(floatcomplex));
    Pt->A_t = (floatcomplex *)malloc(Pt->Nx*Pt->Ny*sizeof(floatcomplex));
    Pt->I_t = (floatcomplex *)malloc(Pt->Nx*Pt->Ny*sizeof(floatcomplex));
    fftwf_plan p_thr_BACKWARD, p_thr_FORWARD, p_thr_xcorr_BACKWARD = NULL, p_thr_xcorr_FORWARD = NULL;
    #ifdef _OPENMP
    #pragma omp critical (fftw)
    #endif
    { // Thread specific FFTW plans
      p_thr_BACKWARD = fftwf_plan_dft_2d(Pt->Ny, Pt->Nx, (fftwf_complex *)Pt->A_t, (fftwf_complex *)Pt->E_t, FFTW_BACKWARD, FFTW_ESTIMATE);
      p_thr_FORWARD = fftwf_plan_dft_2d(Pt->Ny, Pt->Nx, (fftwf_complex *)Pt->E_t, (fftwf_complex *)Pt->A_t, FFTW_FORWARD, FFTW_ESTIMATE);
      if(plotProgress) {
        p_thr_xcorr_BACKWARD = fftwf_plan_dft_2d(Pt->Ny, Pt->Nx, (fftwf_complex *)Pt->A_t, (fftwf_complex *)Pt->I_t, FFTW_BACKWARD, FFTW_ESTIMATE);
        p_thr_xcorr_FORWARD = fftwf_plan_dft_2d(Pt->Ny, Pt->Nx, (fftwf_complex *)Pt->I_t, (fftwf_complex *)Pt->A_t, FFTW_FORWARD, FFTW_ESTIMATE);
      }
    }
    
    #ifdef _OPENMP
    #pragma omp for schedule(dynamic)
    #endif
    for(long iz=0;iz<Pt->Nz;iz++) {
      long iz_PR = -1;
      for(long i=0;i<P->Nz_PR;i++) if(iz == P->iz_PR_array[i]) iz_PR = i; // Find out whether this frame is one that participates in the phase retrieval and if so, what frame number is it in the subset of frames that participate

      if(plotProgress || iz_PR != -1) {
        propagateRefToFrame(Pt,iz);
        fftwf_execute(p_thr_BACKWARD);
      }
      
      if(plotProgress) {
        // Cross correlation to find fidelities
        calcI(Pt);
        fftwf_execute(p_thr_xcorr_FORWARD);
        calcfftprod(Pt, iz);
        fftwf_execute(p_thr_xcorr_BACKWARD);
        findmaxxcorr(Pt, iz); // This calculates the fidelity
        // Cross correlation done
      }

      if(iz_PR != -1) {
        findBestShift(Pt,iz_PR,shiftframes);
        attenuateField(Pt,iz_PR);
        fftwf_execute(p_thr_FORWARD);
        propagateFrameToRef(Pt,iz_PR);
      }
    }
    
    #ifdef _OPENMP
    #pragma omp critical (fftw)
    #endif
    {
      fftwf_destroy_plan(p_thr_BACKWARD);
      fftwf_destroy_plan(p_thr_FORWARD);
      if(plotProgress) {
        fftwf_destroy_plan(p_thr_xcorr_BACKWARD);
        fftwf_destroy_plan(p_thr_xcorr_FORWARD);
      }
    }
    free(Pt->E_t);
    free(Pt->A_t);
    free(Pt->I_t);
  }
  fftwf_execute(p_BACKWARD);

  fftwf_destroy_plan(p_FORWARD);
  fftwf_destroy_plan(p_BACKWARD);
  free(P->A_o);
  #endif
  
  for(long i=0;i<P->Nx*P->Ny;i++) {
    // To begin with we just sum squares of deviations...
    *phaseRMS += (double)sqrf(CARGF(P->E_o[i]/P->E_i[i]));
    *magnitudeRMS += (double)sqrf(CABSF(P->E_o[i]) - CABSF(P->E_i[i]));
    *magEdiffRMS += (double)sqrf(CABSF(P->E_o[i] - P->E_i[i]));
  }
  // ... and then we divide by Nx*Ny to get the mean and take the square root to get RMS
  *phaseRMS = sqrt(*phaseRMS/(P->Nx*P->Ny));
  *magnitudeRMS = sqrt(*magnitudeRMS/(P->Nx*P->Ny));
  *magEdiffRMS = sqrt(*magEdiffRMS/(P->Nx*P->Ny));
}
