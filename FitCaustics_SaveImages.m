function FitCaustics_SaveImages

autorotate = false; % Modify this flag to change whether beam rotation angle gets automatically calculated
rotation = 0; % [degrees] Only used if autorotate = false
displayScaleFactor = 1; % If the images are too large to fit on the screen you can downscale the images by setting this number between 0 and 1.

ignoredframes = []; % List of frame indices to ignore, leave empty to include all frames

[FileName,PathName] = uigetfile('*.mat','Select the .mat file');
FileNameroot = FileName(1:end-4);
fprintf('Loading %s...\n\n',[PathName FileName]);
load([PathName FileName],'I','z_positions','lambda','pixelpitch_x','pixelpitch_y');

if contains(FileName,'phaseretrieved')
  ROI_radius_multiple_of_diameter = Inf;
else
  ROI_radius_multiple_of_diameter = 2.5;
end

I(:,:,ignoredframes) = [];
z_positions(ignoredframes) = [];
[resx,resy,frames] = size(I);

%% Initial loop to determine beam rotation
if(autorotate)
  fprintf(['Determining beam rotation...\n' repmat('-',1,frames)]);
  for idx=1:frames
    if(idx==1)
      freqcomponents = abs(fftshift(fft2(squeeze(I(:,:,idx)))));
    else
      freqcomponents = freqcomponents + abs(fftshift(fft2(squeeze(I(:,:,idx)))));
    end
    fprintf(1,'\b');
  end
  freqcomponents(freqcomponents < 0.01*max(freqcomponents(:))) = 0;
  kx = (-resx/2:resx/2-1)/(resx*pixelpitch_x);
  ky = (-resy/2:resy/2-1)/(resy*pixelpitch_y);
  [Kx,Ky] = ndgrid(kx,ky);
  crossangles = mod(atan2(Ky,Kx),pi/2);
  modulus = sqrt(Kx.^2 + Ky.^2);
  modulus(modulus > kx(end)/4) = 0; % Disregard frequency components corresponding to periods of fewer than 8 pixels
  rotation = -angle(sum(freqcomponents(:).*modulus(:).*exp(1i*crossangles(:)*4)))/4/pi*180;
%   figure(4);
%   imagesc(kx,ky,(freqcomponents.*modulus)');
%   colormap(GPBGYRcolormap);
%   xlim([-4e4 4e4]);
%   ylim([-4e4 4e4]);
%   axis xy
%   axis equal
%   title(num2str(rotation));
end

fprintf(['Rotating frames ' num2str(rotation,'%0.2f') '�\n\n']);

%% Main loop
x_beamdiameters_oneoveresquared = zeros(frames,1);
y_beamdiameters_oneoveresquared = zeros(frames,1);
x_beamdiameters_FWHM = zeros(frames,1);
y_beamdiameters_FWHM = zeros(frames,1);
x_beamdiameters_foursigma = zeros(frames,1);
y_beamdiameters_foursigma = zeros(frames,1);
XVec = 1:resx;
YVec = 1:resy;
[XMat , YMat] = ndgrid(XVec,YVec);
h_f = figure;
h_ax = axes;
if exist([PathName FileNameroot '_images.tiff'],'file')
  delete([PathName FileNameroot '_images.tiff']);
end

for idx=1:frames
  %% Import frame
  frame = imrotate(double(squeeze(I(:,:,idx))),rotation,'bilinear','crop'); % Rotate the image using bilinear interpolation
  
  %% Using 1/e^2 and FWHM thresholds, calculate widths and rough estimate of centroid 
  Power_profile_x = sum(frame,2)';
  Power_profile_y = sum(frame,1);
  
  XVec_overthreshold_oneoveresquared = XVec(Power_profile_x > exp(-2)*max(Power_profile_x));
  YVec_overthreshold_oneoveresquared = YVec(Power_profile_y > exp(-2)*max(Power_profile_y));
  x_beamdiameters_oneoveresquared(idx) = (XVec_overthreshold_oneoveresquared(end)-XVec_overthreshold_oneoveresquared(1)+1)*pixelpitch_x;
  y_beamdiameters_oneoveresquared(idx) = (YVec_overthreshold_oneoveresquared(end)-YVec_overthreshold_oneoveresquared(1)+1)*pixelpitch_y;
  
  XVec_overthreshold_FWHM = XVec(Power_profile_x > 1/2*max(Power_profile_x));
  YVec_overthreshold_FWHM = YVec(Power_profile_y > 1/2*max(Power_profile_y));
  x_beamdiameters_FWHM(idx) = (XVec_overthreshold_FWHM(end)-XVec_overthreshold_FWHM(1)+1)*pixelpitch_x;
  y_beamdiameters_FWHM(idx) = (YVec_overthreshold_FWHM(end)-YVec_overthreshold_FWHM(1)+1)*pixelpitch_y;

  x_centroid_rough = sum(XVec_overthreshold_FWHM.*Power_profile_x(XVec_overthreshold_FWHM))/sum(Power_profile_x(XVec_overthreshold_FWHM));
  y_centroid_rough = sum(YVec_overthreshold_FWHM.*Power_profile_y(YVec_overthreshold_FWHM))/sum(Power_profile_y(YVec_overthreshold_FWHM));
  
  %% Use the above to set ROI ellipse
  distances_to_centroid_units_of_radii = sqrt(((XMat - x_centroid_rough)*pixelpitch_x*2/x_beamdiameters_oneoveresquared(idx)).^2 + ((YMat - y_centroid_rough)*pixelpitch_y*2/y_beamdiameters_oneoveresquared(idx)).^2);
  
  frame_ROI = frame.*(distances_to_centroid_units_of_radii < ROI_radius_multiple_of_diameter);
  
  Power_profile_x_ROI = sum(frame_ROI,2)';
  Power_profile_y_ROI = sum(frame_ROI,1);

  %% Using only data within ROI, calculate centroid more accurately and 4sigma widths
  x_centroid = sum(XVec.*Power_profile_x_ROI)/sum(Power_profile_x_ROI);
  y_centroid = sum(YVec.*Power_profile_y_ROI)/sum(Power_profile_y_ROI);
  
  x_beamdiameters_foursigma(idx) = 4*sqrt(sum((XVec - x_centroid).^2.*Power_profile_x_ROI)/sum(Power_profile_x_ROI))*pixelpitch_x;
  y_beamdiameters_foursigma(idx) = 4*sqrt(sum((YVec - y_centroid).^2.*Power_profile_y_ROI)/sum(Power_profile_y_ROI))*pixelpitch_y;
  
  %% Plot beam profile, power profiles and ROI
  imagesc(h_ax,XVec,YVec,max(0,frame/max(frame(:)))');
  colormap(GPBGYRcolormap);
  hold on
  line(h_ax,1:length(XVec),Power_profile_x/max(Power_profile_x)*0.25*length(YVec)+1,'color','w')
  line(h_ax,Power_profile_y/max(Power_profile_y)*0.25*length(XVec)+1,1:length(YVec),'color','w')

  if isfinite(ROI_radius_multiple_of_diameter)
    x = x_centroid_rough + ROI_radius_multiple_of_diameter*x_beamdiameters_oneoveresquared(idx)/2*cos(linspace(0,2*pi,1000))/pixelpitch_x;
    x = min(resx,max(1,x));
    y = y_centroid_rough + ROI_radius_multiple_of_diameter*y_beamdiameters_oneoveresquared(idx)/2*sin(linspace(0,2*pi,1000))/pixelpitch_y;
    y = min(resy,max(1,y));
    plot(h_ax,x,y,'y-')
  end
  
  axis xy
  axis off
  h_ax.Position = [0 0 1 1];
  h_f.Position = [30 75 resx*displayScaleFactor resy*displayScaleFactor];
  text(20,40,sprintf('z = %.1f mm',1000*z_positions(idx)),'Color','w','FontSize',16);

  drawnow;
  framestruct = getframe(h_ax);
  saved = false;
  while ~saved
    try
      imwrite(framestruct.cdata,[PathName FileNameroot '_images.tiff'],'WriteMode','append');
      saved = true;
    catch
%       fprintf('%s_images.tiff could not be opened, retrying...\n',[PathName FileNameroot]);
      pause(0.5);
    end
  end
end

fittypeobject = fittype('2*sqrt((d_0/2)^2 + Msquared^2*(lambda/(pi*d_0/2))^2*(z - z_0)^2)','problem',{'lambda' , 'd_0'},'independent','z');

%% Foursigma caustic fitting and plotting
[d_x_foursigma_0 , min_x_foursigma_index] = min(x_beamdiameters_foursigma);
z_x_foursigma_0_initial = z_positions(min_x_foursigma_index);
Msquaredguess_x_foursigma = pi*d_x_foursigma_0*x_beamdiameters_foursigma(end)/((z_positions(end) - z_x_foursigma_0_initial)*4*lambda);
fitobject_x_foursigma = fit(z_positions',x_beamdiameters_foursigma,fittypeobject,'StartPoint',[Msquaredguess_x_foursigma z_x_foursigma_0_initial],'problem',{lambda d_x_foursigma_0});
coefficients_x_foursigma = coeffvalues(fitobject_x_foursigma);
z_x_foursigma_0 = coefficients_x_foursigma(2);
Msquared_x_foursigma = coefficients_x_foursigma(1);

[d_y_foursigma_0 , min_y_foursigma_index] = min(y_beamdiameters_foursigma);
z_y_foursigma_0_initial = z_positions(min_y_foursigma_index);
Msquaredguess_y_foursigma = pi*d_y_foursigma_0*y_beamdiameters_foursigma(end)/((z_positions(end) - z_y_foursigma_0_initial)*4*lambda);
fitobject_y_foursigma = fit(z_positions',y_beamdiameters_foursigma,fittypeobject,'StartPoint',[Msquaredguess_y_foursigma z_y_foursigma_0_initial],'problem',{lambda d_y_foursigma_0});
coefficients_y_foursigma = coeffvalues(fitobject_y_foursigma);
z_y_foursigma_0 = coefficients_y_foursigma(2);
Msquared_y_foursigma = coefficients_y_foursigma(1);

fprintf('4sigma diameter on profiler lens: %.1f mm, %.1f mm\n',1e3*fitobject_x_foursigma(0),1e3*fitobject_y_foursigma(0));

h_fig2 = figure;
hold on
plot(z_positions,[x_beamdiameters_foursigma y_beamdiameters_foursigma],'LineStyle','none','Marker','o','MarkerSize',7)
plot(fitobject_x_foursigma)
plot(fitobject_y_foursigma)
title('4\sigma caustics');
xlabel('Distance to lens [m]');
ylabel('Beam 4\sigma diameter [m]');
legend(['M^2_x = ' num2str(Msquared_x_foursigma,'%.2f')],['M^2_y = ' num2str(Msquared_y_foursigma,'%.2f')],'Location','best');
yl = ylim;
ylim([0 yl(2)]);
box on
grid on
grid minor
saveas(h_fig2,[PathName FileNameroot '_4sigmacaustic.png']);
fprintf('Figure saved as %s\n\n',[PathName FileNameroot '_4sigmacaustic.png']);

%% 1/e^2 caustic fitting and plotting
[d_x_oneoveresquared_0 , min_x_oneoveresquared_index] = min(x_beamdiameters_oneoveresquared);
z_x_oneoveresquared_0_initial = z_positions(min_x_oneoveresquared_index);
Msquaredguess_x_oneoveresquared = pi*d_x_oneoveresquared_0*x_beamdiameters_oneoveresquared(end)/((z_positions(end) - z_x_oneoveresquared_0_initial)*4*lambda);
fitobject_x_oneoveresquared = fit(z_positions',x_beamdiameters_oneoveresquared,fittypeobject,'StartPoint',[Msquaredguess_x_oneoveresquared z_x_oneoveresquared_0_initial],'problem',{lambda d_x_oneoveresquared_0});
coefficients_x_oneoveresquared = coeffvalues(fitobject_x_oneoveresquared);
z_x_oneoveresquared_0 = coefficients_x_oneoveresquared(2);
Msquared_x_oneoveresquared = coefficients_x_oneoveresquared(1);

[d_y_oneoveresquared_0 , min_y_oneoveresquared_index] = min(y_beamdiameters_oneoveresquared);
z_y_oneoveresquared_0_initial = z_positions(min_y_oneoveresquared_index);
Msquaredguess_y_oneoveresquared = pi*d_y_oneoveresquared_0*y_beamdiameters_oneoveresquared(end)/((z_positions(end) - z_y_oneoveresquared_0_initial)*4*lambda);
fitobject_y_oneoveresquared = fit(z_positions',y_beamdiameters_oneoveresquared,fittypeobject,'StartPoint',[Msquaredguess_y_oneoveresquared z_y_oneoveresquared_0_initial],'problem',{lambda d_y_oneoveresquared_0});
coefficients_y_oneoveresquared = coeffvalues(fitobject_y_oneoveresquared);
z_y_oneoveresquared_0 = coefficients_y_oneoveresquared(2);
Msquared_y_oneoveresquared = coefficients_y_oneoveresquared(1);

fprintf('1/e^2 diameter on profiler lens: %.1f mm, %.1f mm\n',1e3*fitobject_x_oneoveresquared(0),1e3*fitobject_y_oneoveresquared(0));

h_fig3 = figure;
hold on
plot(z_positions,[x_beamdiameters_oneoveresquared y_beamdiameters_oneoveresquared],'LineStyle','none','Marker','o','MarkerSize',7)
plot(fitobject_x_oneoveresquared)
plot(fitobject_y_oneoveresquared)
title('1/e^{2} caustics');
xlabel('Distance to lens [m]');
ylabel('Beam 1/e^{2} diameter [m]');
legend(['M^2_x = ' num2str(Msquared_x_oneoveresquared,'%.2f')],['M^2_y = ' num2str(Msquared_y_oneoveresquared,'%.2f')],'Location','best');
yl = ylim;
ylim([0 yl(2)]);
box on
grid on
grid minor
saveas(h_fig3,[PathName FileNameroot '_oneoveresquaredcaustic.png']);
fprintf('Figure saved as %s\n\n',[PathName FileNameroot '_oneoveresquaredcaustic.png']);

%% FWHM caustic fitting and plotting
fittypeobject_FWHM = fittype('sqrt(log(2)*2)*sqrt((d_0/sqrt(2*log(2)))^2 + Msquared^2*(lambda/(pi*d_0/sqrt(2*log(2))))^2*(z - z_0)^2)','problem',{'lambda' , 'd_0'},'independent','z');

[d_x_FWHM_0 , min_x_FWHM_index] = min(x_beamdiameters_FWHM);
z_x_FWHM_0_initial = z_positions(min_x_FWHM_index);
Msquaredguess_x_FWHM = 2/log(2)*pi*d_x_FWHM_0*x_beamdiameters_FWHM(end)/((z_positions(end) - z_x_FWHM_0_initial)*4*lambda);
fitobject_x_FWHM = fit(z_positions',x_beamdiameters_FWHM,fittypeobject_FWHM,'StartPoint',[Msquaredguess_x_FWHM z_x_FWHM_0_initial],'problem',{lambda d_x_FWHM_0});
coefficients_x_FWHM = coeffvalues(fitobject_x_FWHM);
z_x_FWHM_0 = coefficients_x_FWHM(2);
Msquared_x_FWHM = coefficients_x_FWHM(1);

[d_y_FWHM_0 , min_y_FWHM_index] = min(y_beamdiameters_FWHM);
z_y_FWHM_0_initial = z_positions(min_y_FWHM_index);
Msquaredguess_y_FWHM = 2/log(2)*pi*d_y_FWHM_0*y_beamdiameters_FWHM(end)/((z_positions(end) - z_y_FWHM_0_initial)*4*lambda);
fitobject_y_FWHM = fit(z_positions',y_beamdiameters_FWHM,fittypeobject_FWHM,'StartPoint',[Msquaredguess_y_FWHM z_y_FWHM_0_initial],'problem',{lambda d_y_FWHM_0});
coefficients_y_FWHM = coeffvalues(fitobject_y_FWHM);
z_y_FWHM_0 = coefficients_y_FWHM(2);
Msquared_y_FWHM = coefficients_y_FWHM(1);

fprintf('FWHM on profiler lens: %.1f mm, %.1f mm\n',1e3*fitobject_x_FWHM(0),1e3*fitobject_y_FWHM(0));

h_fig4 = figure;
hold on
plot(z_positions,[x_beamdiameters_FWHM y_beamdiameters_FWHM],'LineStyle','none','Marker','o','MarkerSize',7)
plot(fitobject_x_FWHM)
plot(fitobject_y_FWHM)
title('FWHM caustics');
xlabel('Distance to lens [m]');
ylabel('Beam FWHM [m]');
legend(['M^2_x = ' num2str(Msquared_x_FWHM,'%.2f')],['M^2_y = ' num2str(Msquared_y_FWHM,'%.2f')],'Location','best');
yl = ylim;
ylim([0 yl(2)]);
box on
grid on
grid minor
saveas(h_fig4,[PathName FileNameroot '_FWHMcaustic.png']);
fprintf('Figure saved as %s\n\n',[PathName FileNameroot '_FWHMcaustic.png']);

%% Save results
save([PathName FileNameroot '_causticresult.mat'],'d_x_foursigma_0','z_x_foursigma_0','Msquared_x_foursigma',...
                            'd_y_foursigma_0','z_y_foursigma_0','Msquared_y_foursigma',...
                            'd_x_oneoveresquared_0','z_x_oneoveresquared_0','Msquared_x_oneoveresquared',...
                            'd_y_oneoveresquared_0','z_y_oneoveresquared_0','Msquared_y_oneoveresquared',...
                            'd_x_FWHM_0','z_x_FWHM_0','Msquared_x_FWHM',...
                            'd_y_FWHM_0','z_y_FWHM_0','Msquared_y_FWHM');
fprintf('Fitting results saved as %s\n\n',[PathName FileNameroot '_causticresult.mat']);

end
