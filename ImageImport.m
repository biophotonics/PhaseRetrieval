function imageImport
% This function imports intensity profile information from image files and
% saves them in a format suitable for subsequently running PhaseRetrieval

lambda = input('What is the wavelength [m]? ');
pixelpitch_x = input('What is the pixel pitch in the x direction (detector x size divided by x resolution) [m]? ');
pixelpitch_y = input('What is the pixel pitch in the y direction (detector y size divided by y resolution) [m]? ');

disp('Select all of the image files (use shift- or ctrl-click)');
[FileName,PathName] = uigetfile('*.*','Select all of the image files (use shift- or ctrl-click)','multiselect','on');
Nz = length(FileName);
[Ny,Nx] = size(imread([PathName , FileName{1}]));
I = zeros(Nx,Ny,Nz);
z_positions = zeros(1,Nz);

for iz = 1:Nz
  I(:,:,iz) = rot90(imread([PathName , FileName{iz}]),-1);
  z_positions(iz) = input(['What is the z position of the frame called ' FileName{iz} ' [m]? ']);
end

save([PathName 'frameData.mat'],'I','z_positions','lambda','pixelpitch_x','pixelpitch_y');
end